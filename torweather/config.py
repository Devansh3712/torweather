#!/usr/bin/env python
"""Module for parsing and validating environment variables."""
from pydantic import BaseSettings


class Secrets(BaseSettings):
    """Class for parsing and validating environment variables
    from `.env` file."""

    EMAIL: str
    PASSWORD: str
    DATABASE_URL: str
    POSTGRES_USER: str
    POSTGRES_PASS: str
    HOST: str
    DATABASE: str

    class Config:
        env_file = ".env"


secrets = Secrets()
