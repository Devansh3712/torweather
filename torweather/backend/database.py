#!/usr/bin/env python
from typing import Any
from typing import List
from typing import Optional
from typing import Tuple

import psycopg2
from psycopg2.sql import Identifier
from psycopg2.sql import SQL

from torweather.config import secrets
from torweather.schemas import Notif


class Database:
    """Class for handling PostgreSQL database operations."""

    def __init__(self, fingerprint: Optional[str] = None) -> None:
        self.fingerprint = fingerprint
        # self.__check_database()
        # self.__connection = psycopg2.connect(
        #     user=secrets.POSTGRES_USER,
        #     password=secrets.POSTGRES_PASS,
        #     host=secrets.HOST,
        #     database=secrets.DATABASE,
        # )
        self.__connection = psycopg2.connect(secrets.DATABASE_URL)
        self.__connection.autocommit = True
        self.__cursor = self.__connection.cursor()
        self.notifs = [notif.name.lower() for notif in Notif]
        self.__setup_database()

    @property
    def cursor(self):
        """Returns the cursor object."""
        return self.__cursor

    # def __check_database(self) -> None:
    #     """If the torweather database doesn't exist, create it."""
    #     connection = psycopg2.connect(
    #         user=secrets.POSTGRES_USER,
    #         password=secrets.POSTGRES_PASS,
    #         host="localhost",
    #     )
    #     connection.autocommit = True
    #     cursor = connection.cursor()
    #     try:
    #         # Create the torweather database if it doesn't exist.
    #         cursor.execute("""CREATE DATABASE %s""", (secrets.DATABASE,))
    #     except:
    #         ...
    #     connection.close()

    def __setup_database(self) -> None:
        """Create tables for users and all types of notifications."""
        users = """
        CREATE TABLE IF NOT EXISTS users (
            id SERIAL UNIQUE,
            fingerprint CHAR(40) PRIMARY KEY,
            email VARCHAR(320) NOT NULL
        )
        """
        self.cursor.execute(users)
        for notif in self.notifs:
            table = """
            CREATE TABLE IF NOT EXISTS {notif} (
                id SERIAL REFERENCES users (id),
                sent bool NOT NULL
            )
            """
            self.cursor.execute(SQL(table).format(notif=Identifier(notif)))
        try:
            self.cursor.execute(
                """
                ALTER TABLE node_down
                ADD COLUMN duration int NOT NULL
                """
            )
        except:
            ...

    @property
    def fingerprints(self) -> List[Tuple[Any, ...]]:
        """Returns list of all fingerprints in users table."""
        query = """SELECT fingerprint FROM users"""
        self.cursor.execute(query)
        return self.cursor.fetchall()

    @property
    def duration(self) -> int:
        """Returns duration before sending a notification for the current relay."""
        query = """
        SELECT node_down.duration
        FROM node_down, users
        WHERE node_down.id = users.id AND users.fingerprint = %s
        """
        self.cursor.execute(query, (self.fingerprint,))
        return self.cursor.fetchone()[0]

    @property
    def email(self) -> str:
        """Returns the subscribed email of the current relay."""
        query = """
        SELECT email
        FROM users
        WHERE fingerprint = %s
        """
        self.cursor.execute(query, (self.fingerprint,))
        return self.cursor.fetchall()[0]

    @property
    def notifications(self) -> List[Tuple[Any, ...]]:
        """Returns list of all notification types."""
        query = """
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema = 'public'
        """
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def insert(self, email: str, duration: int) -> None:
        """Insert user data into the database.

        Args
            email (str): Email to subscribe to.
            duration (int): Duration before sending a notification (hours).
        """
        user = """
        INSERT INTO users (fingerprint, email)
        VALUES (%s, %s)
        """
        self.cursor.execute(user, (self.fingerprint, email))
        for notif in self.notifs:
            if notif == "node_down":
                table = """
                INSERT INTO {notif} (sent, duration)
                VALUES (false, %s)
                """
                self.cursor.execute(
                    SQL(table).format(notif=Identifier(notif)), (duration,)
                )
            else:
                table = """INSERT INTO {notif} (sent) VALUES (false)"""
                self.cursor.execute(SQL(table).format(notif=Identifier(notif)))

    def update(self, notif: str, status: bool) -> None:
        """Update notification sent status.

        Args
            notif (str): Notification to update.
            status (bool): Status to update to.
        """
        query = """
        UPDATE {notif}
        SET sent=%s
        WHERE id=(
            SELECT users.id
            FROM users, {notif}
            WHERE users.id = {notif}.id AND users.fingerprint = %s
        )
        """
        self.cursor.execute(
            SQL(query).format(notif=Identifier(notif)),
            (
                status,
                self.fingerprint,
            ),
        )

    def delete(self) -> None:
        """Delete user from database."""
        for notif in self.notifs:
            table = """
            DELETE FROM {notif}
            WHERE id=(
                SELECT users.id
                FROM users, {notif}
                WHERE users.id = {notif}.id AND users.fingerprint = %s
            )
            """
            self.cursor.execute(
                SQL(table).format(notif=Identifier(notif)), (self.fingerprint,)
            )
        user = """DELETE FROM users WHERE fingerprint = %s"""
        self.cursor.execute(user, (self.fingerprint,))

    def delete_one(self, notif: str) -> None:
        """Unsubscribe from a single type of notification."""
        query = """
        DELETE FROM {notif}
        WHERE id=(
            SELECT users.id
            FROM users, {notif}
            WHERE users.id = {notif}.id AND users.fingerprint = %s
        )
        """
        self.cursor.execute(
            SQL(query).format(notif=Identifier(notif)), (self.fingerprint,)
        )

    def get_false_notifs(self, notif: str) -> List[Tuple[Any, ...]]:
        query = """
        SELECT users.fingerprint, users.email
        FROM users, {notif}
        WHERE users.id = {notif}.id AND {notif}.sent = false
        """
        self.cursor.execute(SQL(query).format(notif=Identifier(notif)))
        return self.cursor.fetchall()

    def get_true_notifs(self, notif: str) -> List[Tuple[Any, ...]]:
        query = """
        SELECT users.fingerprint, users.email
        FROM users, {notif}
        WHERE users.id = {notif}.id AND {notif}.sent = true
        """
        self.cursor.execute(SQL(query).format(notif=Identifier(notif)))
        return self.cursor.fetchall()
