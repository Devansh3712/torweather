#!/usr/bin/env python
"""Module for fetching, validating relay data and subscribe/unsubscribe them to
Tor Weather service."""
from collections.abc import Sequence
from typing import Any

import requests
from email_validator import validate_email
from requests.structures import CaseInsensitiveDict

from torweather.backend.database import Database
from torweather.exceptions import InvalidEmailError
from torweather.exceptions import InvalidFingerprintError
from torweather.exceptions import NotifNotSubscribedError
from torweather.exceptions import RelayNotSubscribedError
from torweather.exceptions import RelaySubscribedError
from torweather.logger import Logger
from torweather.schemas import Notif
from torweather.schemas import RelayData


class Relay(Logger):
    """Class for fetching data of a relay using the onionoo API and
    managing the PostgreSQL database.

    Attributes:
        fingerprint (str): Fingerprint of the relay.
    """

    def __init__(self, fingerprint: str) -> None:
        """Initializes the Relay class with the fields to be fetched by the
        onionoo API and a custom logger."""
        super().__init__(__name__)
        self.fingerprint = fingerprint
        # Fields to fetch for a relay from the onionoo API.
        self.__fields: Sequence[str] = [
            "nickname",
            "fingerprint",
            "last_seen",
            "running",
            "consensus_weight",
            "last_restarted",
            "bandwidth_rate",
            "effective_family",
            "version_status",
            "recommended_version",
        ]
        self.__url: str = "https://onionoo.torproject.org/details"
        self.__validate_fingerprint()
        self.__database = Database(self.fingerprint)

    @property
    def database(self):
        """Returns the database object."""
        return self.__database

    @property
    def data(self):
        """Fetch data of the relay using the onionoo API.

        Returns:
            RelayData: Pydantic model of relay data.
        """
        with requests.Session() as session:
            session.headers = CaseInsensitiveDict(  # type: ignore
                {
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
                    "(KHTML, like Gecko)Chrome/94.0.4606.81 Safari/537.36"
                }
            )
            response = session.get(
                f"{self.__url}?search={self.fingerprint}&fields={','.join(self.__fields)}"
            )
            result = response.json()["relays"]
        return RelayData(**result[0])

    @property
    def duration(self) -> int:
        self.fingerprint_exists()
        if ("node_down",) not in self.database.notifications:
            raise NotifNotSubscribedError(
                self.data.nickname, self.fingerprint, "NODE_DOWN"
            )
        return self.database.duration

    @property
    def email(self) -> str:
        self.fingerprint_exists()
        return self.database.email

    def __validate_fingerprint(self):
        """Validate whether the relay fingerprint exists.

        Raises:
            InvalidFingerprintError: Relay fingerprint not found on onionoo API.
        """
        with requests.Session() as session:
            session.headers = CaseInsensitiveDict(  # type: ignore
                {
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
                    "(KHTML, like Gecko)Chrome/94.0.4606.81 Safari/537.36"
                }
            )
            response = session.get(f"{self.__url}?search={self.fingerprint}")
            if response.status_code != 200:
                raise InvalidFingerprintError(self.fingerprint)
            result = response.json()["relays"]
            if not result:
                raise InvalidFingerprintError(self.fingerprint)

    def fingerprint_exists(self) -> bool:
        """Checks if the current fingerprint exists in the database."""
        if (self.fingerprint,) not in self.database.fingerprints:
            raise RelayNotSubscribedError(self.data.nickname, self.fingerprint)
        return True

    def subscribe(
        self, email: str, notifs: Sequence[Notif], duration: int = 48
    ) -> bool:
        """Subscribe to the Tor weather service.

        Args:
            email (str): Email of relay provider.
            notifs (Sequence[Notif]): Type(s) of notification(s) to subscribe.
            duration (int): Duration before sending a notification (hours). Defaults to 48.

        Raises:
            InvalidEmailError: Email syntax/DNS server is not valid.
            RelaySubscribedError: Relay fingerprint and email already exists in PostgreSQL database.

        Returns:
            bool: True if relay data is added to database.
        """
        # Validate the email address provided by the relay provider.
        # If the email is in wrong syntax/DNS server doesn't exist
        # it raises an error.
        try:
            validate_email(email)
        except:
            raise InvalidEmailError(email)
        # If the fingerprint exists in the database.
        if (self.fingerprint,) in self.database.fingerprints:
            raise RelaySubscribedError(self.fingerprint)
        for relay in self.data.effective_family:
            Database(relay).insert(email, duration)
        self.logger.info(
            f"Node {self.data.nickname} (fingerprint: {self.fingerprint}) subscribed to "
            f"{', '.join([notif.name for notif in notifs])} notifications."
        )
        return True

    def unsubscribe(self) -> bool:
        """Unsubscribe from the Tor weather service.

        Raises:
            RelayNotSubscribedError: Relay fingerprint not found in PostgreSQL database.

        Returns:
            bool: True if relay document is deleted from database.
        """
        self.fingerprint_exists()
        self.database.delete()
        self.logger.info(
            f"Node {self.data.nickname} (fingerprint: {self.fingerprint}) unsubscribed."
        )
        return True

    def unsubscribe_single(self, notif_type: Notif) -> bool:
        """Unsubscribe from a single type of notification.

        Args:
            notif_type (Notif): The notification type to unsubscribe.

        Raises:
            RelayNotSubscribedError: Relay fingerprint not found in PostgreSQL database.
            NotifNotSubscribedError: Notification type not subscribed by relay provider.

        Returns:
            bool: True if notification is unsubscribed.
        """
        self.fingerprint_exists()
        # Check if the user has subscribed to the notif type.
        if (notif_type.name.lower(),) not in self.database.notifications:
            raise NotifNotSubscribedError(
                self.data.nickname, self.fingerprint, notif_type
            )
        # If only a single notification is subscribed and the user
        # explicitly chooses to unsubscribe from it, delete the whole
        # relay from the database.
        if len(self.database.notifications) > 2:
            self.database.delete_one(notif_type.name.lower())
        else:
            self.unsubscribe()
        return True

    def update_notif_status(self, notif_type: Notif, status: bool = True) -> bool:
        """Update the status of a notification subscribed by the relay operator.

        Args:
            notif_type (Notif): The notification type to update.
            status (bool, optional): Status of notification. Defaults to True.

        Raises:
            RelayNotSubscribedError: Relay fingerprint not found in PostgreSQL database.
            NotifNotSubscribedError: Notification type not subscribed by relay provider.

        Returns:
            bool: True if notification's status is updated in the database.
        """
        self.fingerprint_exists()
        if (notif_type.name.lower(),) not in self.database.notifications:
            raise NotifNotSubscribedError(
                self.data.nickname, self.fingerprint, notif_type
            )
        self.database.update(notif_type.name.lower(), status)
        return True
