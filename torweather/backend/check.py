#!/usr/bin/env python
"""Module for checking relay data, sending emails and upating notification status
in the background using apscheduler."""
from collections.abc import Sequence

from apscheduler.executors.pool import ProcessPoolExecutor
from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.schedulers.background import BackgroundScheduler

from torweather.backend.database import Database
from torweather.backend.email import Email
from torweather.backend.relay import Relay
from torweather.schemas import Notif
from torweather.utils import node_down_duration


class Check:
    """Class for checking and updating relay notification status."""

    def __init__(self):
        """Initializes Check class with a BackgroundScheduler object
        and a PostgreSQL database cursor."""
        self.__database = Database()
        self.__executors = {
            "default": ThreadPoolExecutor(20),
            "processpool": ProcessPoolExecutor(5),
        }
        self.__scheduler = BackgroundScheduler(daemon=True, executors=self.__executors)
        self.scheduler.add_job(self.relay_family, trigger="cron", hour=12)
        self.scheduler.add_job(self.hourly, trigger="interval", hours=1)
        self.scheduler.add_job(self.daily, trigger="cron", hour=0)
        # self.scheduler.add_job(self.monthly, trigger="cron", day="last")

    @property
    def scheduler(self):
        """Returns the scheduler object."""
        return self.__scheduler

    @property
    def database(self):
        """Returns the database object."""
        return self.__database

    def relay_family(self) -> None:
        """If a new relay is added to the effective family of any relay, subscribe
        it to thr service as well."""
        result = self.database.fingerprints
        for data in result:
            relay = Relay(data[0])
            for fingerprint in relay.data.effective_family:
                if (fingerprint,) not in result:
                    Relay(fingerprint).subscribe(relay.email)

    def hourly(self) -> None:
        """Hourly checks of subscribed relays."""

        notif_types: Sequence[str] = [
            "NODE_DOWN",
            # "SECURITY_VULNERABILITY",
            # "DNS_FAILURE",
            # "FLAG_LOST",
            # "DETECT_ISSUES",
            # "REQUIREMENTS",
        ]
        for notif in notif_types:
            cursor = self.database.get_false_notifs(notif.lower())
            for data in cursor:
                relay = Relay(data[0])
                if notif == "NODE_DOWN":
                    if node_down_duration(relay.data) > relay.duration:
                        # getattr(Notif, notif) is used to create the enum type of Notif
                        # using the notification type stored in database.
                        Email(relay.data, data[1], getattr(Notif, notif)).send()
                        relay.update_notif_status(getattr(Notif, notif))
                else:
                    Email(relay.data, data[1], getattr(Notif, notif)).send()
                    relay.update_notif_status(getattr(Notif, notif))

    def daily(self) -> None:
        """Daily checks of subscribed relays."""

        notif_types: Sequence[str] = [
            "OUTDATED_VER",
            # "OPERATOR_EVENTS",
        ]
        for notif in notif_types:
            cursor = self.database.get_false_notifs(notif.lower())
            for data in cursor:
                relay = Relay(data[0])
                if notif == "OUTDATED_VER":
                    if relay.data.version_status in ["unrecommended", "obsolete"]:
                        Email(relay.data, data[1], getattr(Notif, notif)).send()
                        relay.update_notif_status(getattr(Notif, notif))

    def monthly(self) -> None:
        """Monthly checks of subscribed relays."""

        notif_types: Sequence[str] = [
            # "TOP_LIST",
            # "DATA",
            # "SUGGESTIONS",
        ]
