#!/usr/bin/env python
from torweather.backend.relay import Relay
from torweather.schemas import RelayData


class Format:
    """Class for formatting notification messages.

    Attributes:
        relay (RelayData): Data of the relay.
        message (str): Message to format.
    """

    def __init__(self, relay: RelayData, message: str) -> None:
        self.relay = relay
        self.message = message

    def node_down(self) -> str:
        """Format node down notification."""
        self.message: str = self.message.format(
            self.relay.nickname,
            self.relay.fingerprint,
            Relay(self.relay.fingerprint).duration,
            self.relay.last_seen,
        )
        return self.message

    def outdated_ver(self) -> str:
        """Format outdated version of Tor notification."""
        self.message = self.message.format(
            self.relay.nickname,
            self.relay.fingerprint,
            self.relay.version_status,
        )
        return self.message
